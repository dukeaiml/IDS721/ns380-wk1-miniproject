### IDS 721 Individual Project 1: ns380

This project sets up a personal website hosted through GitLab Pages. The website is set up using Zola, a static site generator written is Rust.

## Build Process

First, the website is initialized by running the command
```
zola init
```

Then updates can be made by simply updating the Markdown files in the content folder.

The website can be run locally using the command
```
zola serve
```

Before pushing to the remote server for the first time, the codinfox-zola theme must be added as a git submodule so that the build pipeline knows where to grab the dependency for the theme from, using a command like
```
git submodule add https://github.com/svavs/codinfox-zola.git themes/codinfox-zola
```

Once the website is ready to be deployed, simply `git push` to the remote. This will trigger the CI/CD pipeline so the website will be updated.

To set this up, I created the `.gitlab-ci.yml` file, which indicates to GitLab what actions to take upon a push happening. Specifically, it builds the website using `zola build`, then deploys the built page to the GitLab Pages server.

A demonstration video displaying the process is available here: https://youtu.be/UfDk0aJnWzI

## Usage

Here is a screenshot of the website:

![Screenshot](image.png)

And the link to the site is https://ns380-wk1-miniproject-dukeaiml-ids721-b61441f212dd14258fb0efe13.gitlab.io/
